
from player.parser import *
from r2a.ir2a import IR2A
import time
from base.simple_module import SimpleModule
import math
import json
"""
    Based in article: BOLA: Near-Optimal Bitrate Adaptation for Online Videos
    Student: Felipe Nascimento Rocha - 17/0050084
    Projeto final Redes de Computadores - 2/2020

    IMPORTANTE!!:
    Obs.: é necessário mudar manualmente o valor de 'self.p' para a duracao em segundos de cada segmento escolhido, nesse cdn temos 1,2,4,6,10,15 dependendo da url solicitada.
"""




# BOLA METRICS:


# 1 -  time-average playback quality
# 2 -  fraction of time spent not rebuffering
#
#  Variables:
#   p = segment duration in seconds
#   buffer_size
#   Sm = Video SIZE OF Segment m
#   Tk = tk+1 - tk;   t1 = 0
#
#   am(tk) =
#           1 se o Player baixar um segmento de índice m no slot k,
#           0 caso contrário
#
#    Q(t) = buffer level in time k = ex: 3 ,
#    Q(tk+1) = max[(Qt(k) - Tk/p), 0] +  am(tk)
#
#

# 1. We want a maximum buffer level Q max . = 60


# 2. We want to download at the highest bitrate when the buffer
# level is Q max . if buffer level == max buffer => download max birate


# 3. We want to download at the lowest bitrate when the buffer
# level is less than a threshold Q low , and we want to download at
# a higher bitrate when the buffer level goes above the threshold.
#
#
#
#
#
#
#

class R2A(IR2A):
    def __init__(self, id):
        IR2A.__init__(self, id)
        self.parsed_mpd = ''
        self.qi = []

        #  getting
        # max_buffer_size dynamically from dash_client.json:
        f = open('dash_client.json')
        datastore = json.load(f)
        max_buffer_size = datastore["max_buffer_size"]

        #  segment size in seconds and max buffer size
        # MUDAR AO alterar url dO JSON
        self.p = 1
        self.max_buffer_size = max_buffer_size

        # used to store the req and resp time of each segment downloaded
        self.requestTime = time.perf_counter()
        self.responseTime = time.perf_counter()

        # current buffer size:
        self.buffer_size = 0

        #  lista p/ armazenar o tempo de cada requisicao,  TK = [T1, T2, T3...]
        self.time_list = list()

        #  lista p/ armazenar o valor utiulity dos segmentos,[ am1, am2,am3 ....]
        self.am_list = [0]

        # queue with buffer levels in time [q(t=1), q(t=2), q(t=3)] .... :
        self.buffer_level_queue = [0]

        # used to keep track of iterations from request to response
        self.n = 0

    # methods:

    def handle_xml_request(self, msg):
        print('xml_request', msg.__dict__)
        self.send_down(msg)

    def handle_xml_response(self, msg):
        self.parsed_mpd = parse_mpd(msg.get_payload())
        print('parse_mpd', self.parsed_mpd)
        print('xml_rtepsone', msg.__dict__)

        self.qi = self.parsed_mpd.get_qi()
        self.send_up(msg)

    def handle_segment_size_request(self, msg):
        self.requestTime = time.perf_counter()
        qi_id = self.buffer_level()  # ABR
        self.buffer_level_queue.append(qi_id)
        msg.add_quality_id(self.qi[math.ceil(qi_id)])
        self.send_down(msg)

    def handle_segment_size_response(self, msg):
        self.buffer_size = self.get_current_buffer_size()    
        self.am_list.append(1) 
   
        self.responseTime = time.perf_counter() - self.requestTime
        self.time_list.append(self.responseTime)

        self.n = self.n + 1
        self.send_up(msg)

    def get_Tk_level(self, k):
        return self.time_list[k - 1]

    def get_am(self, k):
        return self.am_list[k-1]

    def get_current_buffer_size(self):
        history = self.whiteboard.get_playback_buffer_size()
        if len(history) > 0:
            return history[len(history) - 1][1] - 1
        else:
            return 0
    def buffer_level(self):

        # initialize buffer in low quality or if buffer is empty set to low quality
        if self.n == 0 or self.get_current_buffer_size() == 0:
            return 0
        # We want to increase bitrate when the buffer level is Q max . if buffer size == max buffer => increase bitrate        #
        elif self.get_current_buffer_size() >= self.max_buffer_size:
            if math.floor(self.buffer_level_queue[self.n]) >= len(self.qi) - 1: # max level
                return -1
            return self.buffer_level_queue[self.n] + 1

        else:
            # next level = max[(current_level - time of req/segment duration), 0] + 1 or 0 if buffer is increasing/not changing(+1) or decreasing(+0) in size
            level = max((self.buffer_level_queue[self.n] - self.get_Tk_level(
                self.n)/self.p), 0) + self.get_am(self.n)
            if level >= len(self.qi) - 1:

                # highest quality set if level is above supported ones (last element of queue qi)
                return len(self.qi) - 1
            return level
    def initialize(self):
        SimpleModule.initialize(self)
        pass

    def finalization(self):
        SimpleModule.finalization(self)
        pass
